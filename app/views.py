from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from app.models import Categoria, Pelicula

lista_categorias = [
            {'id': 1, 'nombre': 'Terror'},
            {'id': 2, 'nombre': 'Romance'},
            {'id': 3, 'nombre': 'Aventuras'},
            {'id': 4, 'nombre': 'Drama'},
            {'id': 5, 'nombre': 'Documental'},
        ]

def index(request):
    return render(request, 'app/index.html')

def categorias(request):
    # Obtiene todas las categorias
    lista_categorias = Categoria.objects.all()

    contexto = {
        'titulo': 'Blockbuster', 
        'categorias': lista_categorias,
    }
    return render(request, 'app/categorias.html', contexto)


def peliculas(request):
    return render(request, 'app/peliculas.html')


def categoria(request, id):
    return HttpResponse(f'Esta es la categoria {id}')


@login_required
def form_crear_categoria(request):
    return render(request, 'app/crearCategoria.html')


@login_required
def crear_categoria_post(request):
    # Obtiene los datos del cliente
    nombre = request.POST['nombre']

    # Crea la categoría en la BD
    categoria = Categoria()
    categoria.nombre = nombre
    categoria.save()

    return redirect('app:categorias')


@login_required
def form_crear_pelicula(request):
    # Obtiene las categorias
    categorias = Categoria.objects.all()
    # Crea el contexto
    contexto = {
        'categorias': categorias
    }
    return render(request, 'app/crearPelicula.html', contexto)


@login_required
def crear_pelicula_post(request):
    # Obtiene los datos del cliente
    titulo = request.POST['titulo']
    id_categoria = int(request.POST['categoria'])

    # Obtiene la categoria
    categoria = Categoria.objects.get(id=id_categoria)

    # Crea la pelicula
    pelicula = Pelicula()
    pelicula.titulo = titulo
    pelicula.categoria = categoria

    # Guarda la pelicula
    pelicula.save()

    return redirect('app:peliculas')


def form_iniciar_sesion(request):
    return render(request, 'app/login.html')


def iniciar_sesion(request):
    # Obtiene los datos del cliente
    username = request.POST['username']
    password = request.POST['password']

    # Obtiene el usuario
    usuario = authenticate(username=username, password=password)

    if usuario is not None:
        # Autentica el usuario en la plataforma
        login(request, usuario)
        return redirect('app:index')
    else:
        return redirect('app:form_iniciar_sesion')


def cerrar_sesion(request):
    logout(request)
    return redirect('app:index')