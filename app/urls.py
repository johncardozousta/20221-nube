from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'), 
    path('login/', views.form_iniciar_sesion, name='form_iniciar_sesion'), 
    path('login_post/', views.iniciar_sesion, name='iniciar_sesion'), 
    path('logout/', views.cerrar_sesion, name='cerrar_sesion'), 

    path('categorias/', views.categorias, name='categorias'), 
    path('categorias/<int:id>/', views.categoria, name='categoria'), 

    path('categorias/crear/', views.form_crear_categoria, name='form_crear_categoria'),
     
    path('categorias/crear_post/', views.crear_categoria_post, name='crear_categoria_post'), 

    path('peliculas/', views.peliculas, name='peliculas'), 
    path('peliculas/crear/', views.form_crear_pelicula, name='form_crear_pelicula'),
    path('peliculas/crear_post/', views.crear_pelicula_post, name='crear_pelicula_post'), 

]